
# EKS Provisioning
## Export Environments
### You can set AWS credentials as env variables from where you run terraform 
```bash
export AWS_ACCESS_KEY_ID="your_access_key"
export AWS_SECRET_ACCESS_KEY="your_secret_key"
```
Terraform will automatically use these environment variables.